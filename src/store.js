import { createStore, combineReducers, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

// reducers
import { transactionReducer } from './reducers/trasactionReducer';
import { cardReducer } from './reducers/cardReducer';

// middlewares;
const middlewares = [thunk];

const reducers = combineReducers({
  transactions: transactionReducer,
  cards: cardReducer,
});

const store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(...middlewares))
);
export default store;
