import React, { useEffect, useMemo, lazy } from 'react';
import { useParams, useLocation, Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import Loading from '../../components/Loading';
import { getSingleTransaction } from '../../actions/transactionActions';
import { formatDate } from '../../utils';
const BreadCrumb = lazy(() => import('../../components/BreadCrumb'));

const TransactionDetail = () => {
  let params = useParams();
  let location = useLocation();
  let dispatch = useDispatch();

  let routes = useMemo(() => {
    if (params.cardId) {
      return [
        { path: '/', name: 'Home', last: false },
        { path: '/cards', name: 'Cards', last: false },
        {
          path: `/cards/${params.cardId}`,
          name: `Card ${params.cardId}`,
          last: false,
        },
        {
          path: `/cards/${params.cardId}/transactions`,
          name: 'Transactions',
          last: false,
        },
        {
          path: `/cards/${params.cardId}/transactions/${params.transactionId}`,
          name: `Transaction ${params.transactionId}`,
          last: false,
        },
      ];
    } else {
      return [
        { path: '/', name: 'Home', last: false },
        { path: '/transactions', name: 'Transactions', last: false },
        {
          path: `/transactions/${params.transactionId}`,
          name: `Transaction ${params.transactionId}`,
          last: true,
        },
      ];
    }
  }, [params]);

  const { transaction, loading } = useSelector((state) => state.transactions);

  useEffect(() => {
    dispatch(getSingleTransaction(params.transactionId));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <div>
          <BreadCrumb routes={routes} />
          <h1 className="mb-4">Transaction Detail</h1>
          {transaction && (
            <div className="card">
              <div className="card-header">{transaction.id} Detail</div>
              <div className="card-body">
                <div className="card">
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item">
                      <span className="fw-bold me-1">amount:</span>{' '}
                      {transaction.amount}
                    </li>
                    <li className="list-group-item">
                      <span className="fw-bold me-1">currency:</span>
                      {transaction.currency}
                    </li>
                    <li className="list-group-item">
                      <span className="fw-bold me-1">card account:</span>
                      {transaction.cardAccount}
                    </li>
                    <li className="list-group-item">
                      <span className="fw-bold me-1">card id:</span>
                      {transaction.cardId}
                      <Link
                        className="btn btn-primary ms-2"
                        to={`${
                          params.cardId && params.transactionId
                            ? `/cards/${params.cardId}`
                            : `${location.pathname}/${transaction.cardId}`
                        }   `}
                      >
                        go to card detail
                      </Link>
                    </li>
                    <li className="list-group-item">
                      <span className="fw-bold me-1">merchant info:</span>
                      {transaction.merchantInfo}
                    </li>
                    <li className="list-group-item">
                      <span className="fw-bold me-1">transaction date:</span>
                      {formatDate(transaction.transactionDate)}
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          )}
        </div>
      )}
    </>
  );
};

export default TransactionDetail;
