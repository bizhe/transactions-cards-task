import React from 'react';
import PropTypes from 'prop-types';

import ShowButton from '../../components/ShowButton';
import { listRowNumbers, formatDate } from '../../utils';

const TransactionTable = ({ data, onShow, currentPage, perPage }) => {
  return (
    <tbody>
      {data.map((item, index) => (
        <tr key={item.id}>
          <td>{listRowNumbers(currentPage, perPage, index)}</td>
          <td>{item.id}</td>
          <td>{item.cardAccount}</td>
          <td>{item.cardId}</td>
          <td>{item.amount}</td>
          <td>{item.currency}</td>
          <td>{formatDate(item.transactionDate)}</td>
          <td>{item.merchantInfo}</td>
          <td>
            <div className="d-flex justify-content-center">
              <ShowButton onAction={() => onShow(item.id)} />
            </div>
          </td>
        </tr>
      ))}
    </tbody>
  );
};

TransactionTable.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  onShow: PropTypes.func,
  currentPage: PropTypes.number.isRequired,
  perPage: PropTypes.number.isRequired,
};

export default TransactionTable;
