import React from 'react';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { Link } from 'react-router-dom';

const BreadCrumb = ({ routes }) => {
  return (
    <>
      <Breadcrumb>
        {routes.map((item) => {
          return (
            <Breadcrumb.Item key={item.name} active={item.last}>
              {item.last ? (
                <span>{item.name}</span>
              ) : (
                <Link to={item.path}>{item.name}</Link>
              )}
            </Breadcrumb.Item>
          );
        })}
      </Breadcrumb>
    </>
  );
};

export default BreadCrumb;
