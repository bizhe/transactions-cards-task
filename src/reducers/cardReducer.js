import {
  GET_CARDS_REQUEST,
  GET_CARDS_SUCCESS,
  GET_CARDS_FAIL,
  GET_SINGLE_CARD_REQUEST,
  GET_SINGLE_CARD_SUCCESS,
  GET_SINGLE_CARD_FAIL,
} from '../constants/types';

export const cardReducer = (state = {}, actions) => {
  switch (actions.type) {
    case GET_CARDS_REQUEST:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case GET_CARDS_SUCCESS:
      return {
        ...state,
        loading: false,
        card_list: actions.payload,
        error: '',
      };
    case GET_CARDS_FAIL:
      return {
        ...state,
        loading: false,
        error: actions.payload,
      };

    case GET_SINGLE_CARD_REQUEST:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case GET_SINGLE_CARD_SUCCESS:
      return {
        ...state,
        loading: false,
        card: actions.payload,
      };
    case GET_SINGLE_CARD_FAIL:
      return {
        ...state,
        loading: false,
        error: actions.payload,
      };

    default:
      return state;
  }
};
