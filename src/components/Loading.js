import React from "react";

const Loading = () => {
  return (
    <div className="spinner active">
      <div className="loadingio-spinner-dual-ring-8auctkuwrqg">
        <div className="ldio-y23xy7v0ui">
          <div></div>
          <div>
            <div></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Loading;
