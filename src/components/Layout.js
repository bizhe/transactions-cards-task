import { NavLink, Outlet } from "react-router-dom";
import { Navbar, Nav } from "react-bootstrap";

function Layout() {
  return (
    <div className="layout">
      <div className="container">
        <Navbar expand="lg">
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto layout--nav">
              <NavLink
                className={({ isActive }) => (isActive ? "active" : "")}
                to={`/`}
              >
                Home
              </NavLink>
              <NavLink
                className={({ isActive }) => (isActive ? "active" : "")}
                to={`/transactions`}
              >
                Transactions
              </NavLink>
              <NavLink
                className={({ isActive }) => (isActive ? "active" : "")}
                to={`/cards`}
              >
                Cards
              </NavLink>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <div className="layout--content">
          <Outlet />
        </div>
      </div>
    </div>
  );
}

export default Layout;
