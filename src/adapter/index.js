import axios from "axios";

const client = axios.create({
  baseURL: "https://json-fake-api.herokuapp.com",
});

client.interceptors.request.use((config) => {
  config.headers = {
    ...config.headers,
  };

  return config;
});

const adapter = function (options) {
  function onSuccess(response) {
    return response.data;
  }

  function onError(error) {
    if (error.response.status === 401) {
      return Promise.reject(error.response || error.message);
    }
    return Promise.reject(error.response || error.message);
  }
  return client(options).then(onSuccess).catch(onError);
};

export default adapter;
