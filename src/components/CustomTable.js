import React from "react";
import Table from "react-bootstrap/Table";
import PropTypes from "prop-types";

const CustomTable = ({ columns, children }) => {
  return (
    <Table responsive striped bordered hover>
      <thead>
        <tr>
          <th>#</th>
          {columns.map((head) => (
            <th key={head}>{head}</th>
          ))}
        </tr>
      </thead>
      {children}
    </Table>
  );
};

CustomTable.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.string),
};

export default CustomTable;
