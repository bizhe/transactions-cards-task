import React, { useEffect, useState, lazy, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';

import Loading from '../../components/Loading';
import CustomPagination from '../../components/CustomPagination';
import CustomTable from '../../components/CustomTable';
import { getTransactions } from '../../actions/transactionActions';

const TransactionTable = lazy(() => import('./TransactionTable'));
const BreadCrumb = lazy(() => import('../../components/BreadCrumb'));

const columns = [
  'Transaction id',
  'Card Account',
  'Card Id',
  'Amount',
  'Currency',
  'Transaction Date',
  'Merchant',
];

const Transactions = (props) => {
  const [activePage, setActivePage] = useState(1);
  const [tableData, setTableData] = useState([]);
  const [count, setCount] = useState(0);

  const { register, handleSubmit, reset, getValues } = useForm();
  const dispatch = useDispatch();
  let navigate = useNavigate();
  let params = useParams();

  const { transaction_list, loading } = useSelector(
    (state) => state.transactions
  );

  useEffect(() => {
    dispatch(getTransactions());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (transaction_list) {
      setTableData(transaction_list.data);
      setCount(transaction_list.count);
    }
  }, [transaction_list]);

  const handlePageChange = (pageNumber) => {
    setActivePage(pageNumber);
    let filters = getValues();
    dispatch(getTransactions({ pageNumber, ...filters }));
  };

  const onSubmit = (data) => {
    const filters = {
      pageNumber: 1,
      amount: data.amount,
      currency: data.currency,
      cardId: data.cardId,
      cardAccount: data.cardAccount,
      merchantInfo: data.merchantInfo,
    };

    setActivePage(1);
    dispatch(getTransactions(filters));
  };

  const handleReset = () => {
    reset();
    setActivePage(1);
    dispatch(getTransactions());
  };

  const handleShowDetail = (id) => {
    if (params.cardId) {
      navigate(`/cards/${params.cardId}/transactions/${id}`);
    } else {
      navigate(`/transactions/${id}`);
    }
  };

  let routes = useMemo(() => {
    if (params.cardId) {
      return [
        { path: '/', name: 'Home', last: false },
        { path: '/cards', name: 'Cards', last: false },
        {
          path: `/cards/${params.cardId}`,
          name: `Card ${params.cardId}`,
          last: false,
        },
        {
          path: `/cards/${params.cardId}/transactions`,
          name: `Transactions`,
          last: true,
        },
      ];
    } else {
      return [
        { path: '/', name: 'Home', last: false },
        { path: '/transactions', name: 'Transactions', last: true },
      ];
    }
  }, [params]);

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <div>
          <BreadCrumb routes={routes} />
          <h1 className="mb-4">Transactions</h1>
          <form className="bg-light p-3 mb-3" onSubmit={handleSubmit(onSubmit)}>
            <div className="row no-gutters">
              <div className="col-md-2">
                <div>
                  <label className="form-label" htmlFor="cardId">
                    Card Id
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="cardId"
                    placeholder="card id..."
                    {...register('cardId')}
                  />
                </div>
              </div>
              <div className="col-md-2">
                <div>
                  <label className="form-label" htmlFor="cardAccount">
                    Card Account
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="cardAccount"
                    placeholder="card account..."
                    {...register('cardAccount')}
                  />
                </div>
              </div>
              <div className="col-md-2">
                <div>
                  <label className="form-label" htmlFor="amount">
                    Amount
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="amount"
                    placeholder="amount..."
                    {...register('amount')}
                  />
                </div>
              </div>
              <div className="col-md-2">
                <div>
                  <label className="form-label" htmlFor="currency">
                    Currency
                  </label>
                  <select
                    className="form-select"
                    id="currency"
                    {...register('currency')}
                  >
                    <option value="">select</option>
                    <option value="AZN">AZN</option>
                    <option value="USD">USD</option>
                    <option value="EUR">EUR</option>
                  </select>
                </div>
              </div>
              <div className="col-md-2">
                <div>
                  <label className="form-label" htmlFor="merchant">
                    Merchant
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="merchant"
                    placeholder="merchant..."
                    {...register('merchantInfo')}
                  />
                </div>
              </div>
            </div>
            <div className="row mt-3">
              <div className="col-md-3">
                <button type="submit" className="btn btn-primary">
                  Search
                </button>
                <button
                  onClick={handleReset}
                  className="btn btn-info text-white ms-3"
                >
                  Reset
                </button>
              </div>
            </div>
          </form>
          <CustomTable columns={columns}>
            <TransactionTable
              data={tableData}
              onShow={handleShowDetail}
              currentPage={activePage}
              perPage={10}
            />
          </CustomTable>
          <CustomPagination
            onChange={handlePageChange}
            active={activePage}
            itemCount={count}
          />
        </div>
      )}
    </>
  );
};

export default Transactions;
