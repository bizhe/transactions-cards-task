import React from "react";
import Pagination from "react-js-pagination";
import PropTypes from "prop-types";

const CustomPagination = ({ onChange, active, itemCount }) => {
  return (
    <>
      {itemCount !== 0 && (
        <section className="pagination-wrapper">
          <div className="pagination v-desktop">
            <Pagination
              activePage={active}
              itemsCountPerPage={10}
              totalItemsCount={itemCount}
              pageRangeDisplayed={10}
              onChange={onChange}
              activeLinkClass={"active"}
            />
          </div>
          <div className="pagination v-mob">
            <Pagination
              activePage={active}
              itemsCountPerPage={10}
              totalItemsCount={itemCount}
              pageRangeDisplayed={3}
              onChange={onChange}
              activeLinkClass={"active"}
            />
          </div>
        </section>
      )}
    </>
  );
};

CustomPagination.propTypes = {
  itemCount: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  active: PropTypes.number.isRequired,
};

export default CustomPagination;
