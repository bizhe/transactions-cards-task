import {
  GET_CARDS_REQUEST,
  GET_CARDS_SUCCESS,
  GET_CARDS_FAIL,
  GET_SINGLE_CARD_REQUEST,
  GET_SINGLE_CARD_SUCCESS,
  GET_SINGLE_CARD_FAIL,
} from '../constants/types';
import adapter from '../adapter';

export const getCards =
  ({ pageNumber = 1, pageSize = 9, status, currency, id, cardAccount } = {}) =>
  async (dispatch) => {
    dispatch({ type: GET_CARDS_REQUEST });

    let parameters = {
      status,
      currency,
      id,
      cardAccount,
    };

    for (const key of Object.keys(parameters)) {
      if (parameters[key] === '') {
        delete parameters[key];
      }
    }

    try {
      const result = await adapter({
        url: `/cards`,
        method: 'GET',
        params: {
          _page: pageNumber,
          _limit: pageSize,
          ...parameters,
        },
      });

      const countR = await adapter({
        url: `/cards`,
        method: 'GET',
        params: parameters,
      });

      dispatch({
        type: GET_CARDS_SUCCESS,
        payload: { data: result, count: countR.length },
      });
    } catch (e) {
      dispatch({ type: GET_CARDS_FAIL, payload: e });
    }
  };

export const getSingleCard = (id) => async (dispatch) => {
  dispatch({ type: GET_SINGLE_CARD_REQUEST });
  try {
    const result = await adapter({
      url: `/cards/${id}`,
      method: 'GET',
    });

    dispatch({ type: GET_SINGLE_CARD_SUCCESS, payload: result });
  } catch (e) {
    dispatch({ type: GET_SINGLE_CARD_FAIL, payload: e });
  }
};
