import React from "react";
import PropTypes from "prop-types";
import { AiOutlineEye } from "react-icons/ai";

const ShowButton = ({ onAction }) => {
  return (
    <button
      onClick={onAction}
      className="btn btn-default text-white action-icon-box"
    >
      <AiOutlineEye size={18} />
    </button>
  );
};

ShowButton.propTypes = {
  onAction: PropTypes.func,
};

export default ShowButton;
