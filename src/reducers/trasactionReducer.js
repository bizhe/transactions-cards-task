import {
  GET_TRANSACTIONS_REQUEST,
  GET_TRANSACTIONS_SUCCESS,
  GET_TRANSACTIONS_FAIL,
  GET_SINGLE_TRANSACTION_REQUEST,
  GET_SINGLE_TRANSACTION_SUCCESS,
  GET_SINGLE_TRANSACTION_FAIL,
} from '../constants/types';

export const transactionReducer = (state = {}, actions) => {
  switch (actions.type) {
    case GET_TRANSACTIONS_REQUEST:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case GET_TRANSACTIONS_SUCCESS:
      return {
        ...state,
        loading: false,
        transaction_list: actions.payload,
        error: '',
      };
    case GET_TRANSACTIONS_FAIL:
      return {
        ...state,
        loading: false,
        error: actions.payload,
      };

    case GET_SINGLE_TRANSACTION_REQUEST:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case GET_SINGLE_TRANSACTION_SUCCESS:
      return {
        ...state,
        loading: false,
        transaction: actions.payload,
      };
    case GET_SINGLE_TRANSACTION_FAIL:
      return {
        ...state,
        loading: false,
        error: actions.payload,
      };

    default:
      return state;
  }
};
