export const listRowNumbers = (currentPage, pageSize, index) => {
  return (currentPage - 1) * pageSize + index + 1;
};

export const formatDate = (date) => {
  return new Date(date).toLocaleDateString();
};
