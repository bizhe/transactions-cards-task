import React, { useEffect, useState, lazy } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';

import Loading from '../../components/Loading';
import CustomPagination from '../../components/CustomPagination';
import { getCards } from '../../actions/cardActions';
const BreadCrumb = lazy(() => import('../../components/BreadCrumb'));
const CardBox = lazy(() => import('./CardBox.js'));

const Cards = (props) => {
  const [activePage, setActivePage] = useState(1);
  const [count, setCount] = useState(0);
  const [data, setData] = useState([]);

  const { register, handleSubmit, reset, getValues } = useForm();
  const dispatch = useDispatch();
  let navigate = useNavigate();

  const { card_list, loading } = useSelector((state) => state.cards);

  useEffect(() => {
    dispatch(getCards());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (card_list) {
      setData(card_list.data);
      setCount(card_list.count);
    }
  }, [card_list]);

  const handlePageChange = (pageNumber) => {
    setActivePage(pageNumber);
    let filters = getValues();
    dispatch(getCards({ pageNumber, ...filters }));
  };

  const onSubmit = (data) => {
    const filters = {
      pageNumber: 1,
      status: data.status,
      currency: data.currency,
      id: data.cardId,
      cardAccount: data.cardAccount,
    };

    setActivePage(1);
    dispatch(getCards(filters));
  };

  const handleReset = () => {
    reset();
    setActivePage(1);
    dispatch(getCards());
  };

  const handleShowDetail = (id) => {
    navigate(`/cards/${id}`);
  };

  let routes = [
    { path: '/', name: 'Home', last: false },
    { path: '/cards', name: 'Cards', last: true },
  ];

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <div>
          <BreadCrumb routes={routes} />
          <h1 className="mb-4">Cards</h1>
          <form className="bg-light p-3 mb-3" onSubmit={handleSubmit(onSubmit)}>
            <div className="row no-gutters">
              <div className="col-md-2">
                <div>
                  <label className="form-label" htmlFor="cardId">
                    Card Id
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="cardId"
                    placeholder="card id..."
                    {...register('cardId')}
                  />
                </div>
              </div>
              <div className="col-md-2">
                <div>
                  <label className="form-label" htmlFor="cardAccount">
                    Card Account
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="cardAccount"
                    placeholder="card account..."
                    {...register('cardAccount')}
                  />
                </div>
              </div>
              <div className="col-md-2">
                <div>
                  <label className="form-label" htmlFor="status">
                    Status
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="status"
                    placeholder="status..."
                    {...register('status')}
                  />
                </div>
              </div>
              <div className="col-md-2">
                <div>
                  <label className="form-label" htmlFor="currency">
                    Currency
                  </label>
                  <select
                    className="form-select"
                    id="currency"
                    {...register('currency')}
                  >
                    <option value="">select</option>
                    <option value="AZN">AZN</option>
                    <option value="USD">USD</option>
                    <option value="EUR">EUR</option>
                  </select>
                </div>
              </div>
            </div>
            <div className="row mt-3">
              <div className="col-md-3">
                <button type="submit" className="btn btn-primary">
                  Search
                </button>
                <button
                  onClick={handleReset}
                  className="btn btn-info text-white ms-3"
                >
                  Reset
                </button>
              </div>
            </div>
          </form>
          <div className="row">
            {data.map((item) => {
              return (
                <CardBox
                  key={item.id}
                  onAction={handleShowDetail}
                  data={item}
                />
              );
            })}
          </div>
          <CustomPagination
            onChange={handlePageChange}
            active={activePage}
            itemCount={count}
          />
        </div>
      )}
    </>
  );
};

export default Cards;
