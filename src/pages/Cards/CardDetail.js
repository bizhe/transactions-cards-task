import React, { useEffect, lazy, useMemo } from 'react';
import { useParams, useLocation, Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import Loading from '../../components/Loading';
import { getSingleCard } from '../../actions/cardActions';
import { formatDate } from '../../utils';

const BreadCrumb = lazy(() => import('../../components/BreadCrumb'));

const CardDetail = () => {
  let params = useParams();
  let location = useLocation();
  let dispatch = useDispatch();

  const { card, loading } = useSelector((state) => state.cards);

  useEffect(() => {
    dispatch(getSingleCard(params.cardId));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  let routes = useMemo(() => {
    if (params.transactionId) {
      return [
        { path: '/', name: 'Home', last: false },
        { path: '/transactions', name: 'Transactions', last: false },
        {
          path: `/transactions/${params.transactionId}`,
          name: `Transactions ${params.transactionId}`,
          last: false,
        },
        {
          path: `/cards/${params.cardId}`,
          name: `Card ${params.cardId}`,
          last: true,
        },
      ];
    } else {
      return [
        { path: '/', name: 'Home', last: false },
        { path: '/cards', name: 'Cards', last: false },
        {
          path: `/cards/${params.cardId}`,
          name: `Card ${params.cardId}`,
          last: true,
        },
      ];
    }
  }, [params]);

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <div>
          <BreadCrumb routes={routes} />
          <h1 className="mb-4"> Card Detail</h1>
          <Link
            className="btn btn-primary mb-4"
            to={`${
              params.transactionId
                ? '/transactions'
                : `${location.pathname}/transactions`
            }`}
          >
            Go to transaction page
          </Link>
          {card && (
            <div className="card">
              <div className="card-header">{card.id} Detail</div>
              <div className="card-body">
                <div className="card">
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item">
                      <span className="fw-bold me-1">status:</span>{' '}
                      {card.status}
                    </li>
                    <li className="list-group-item">
                      <span className="fw-bold me-1">currency:</span>
                      {card.currency}
                    </li>
                    <li className="list-group-item">
                      <span className="fw-bold me-1">card account:</span>
                      {card.cardAccount}
                    </li>
                    <li className="list-group-item">
                      <span className="fw-bold me-1">card id:</span>
                      {card.id}
                    </li>
                    <li className="list-group-item">
                      <span className="fw-bold me-1">balance:</span>
                      {card.balance}
                    </li>
                    <li className="list-group-item">
                      <span className="fw-bold me-1">expire date:</span>
                      {formatDate(card.expireDate)}
                    </li>
                    <li className="list-group-item">
                      <span className="fw-bold me-1">card number:</span>
                      **** **** **** {card.maskedCardNumber}
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          )}
        </div>
      )}
    </>
  );
};

export default CardDetail;
