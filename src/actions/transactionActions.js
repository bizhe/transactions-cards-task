import {
  GET_TRANSACTIONS_REQUEST,
  GET_TRANSACTIONS_SUCCESS,
  GET_TRANSACTIONS_FAIL,
  GET_SINGLE_TRANSACTION_REQUEST,
  GET_SINGLE_TRANSACTION_SUCCESS,
  GET_SINGLE_TRANSACTION_FAIL,
} from '../constants/types';
import adapter from '../adapter';

export const getTransactions =
  ({
    pageNumber = 1,
    pageSize = 10,
    amount,
    currency,
    cardId,
    cardAccount,
    marchantInfo,
  } = {}) =>
  async (dispatch) => {
    dispatch({ type: GET_TRANSACTIONS_REQUEST });

    let parameters = {
      amount,
      currency,
      cardId,
      cardAccount,
      marchantInfo,
    };

    for (const key of Object.keys(parameters)) {
      if (parameters[key] === '') {
        delete parameters[key];
      }
    }

    try {
      const result = await adapter({
        url: `/transactions`,
        method: 'GET',
        params: {
          _page: pageNumber,
          _limit: pageSize,
          ...parameters,
        },
      });

      const countR = await adapter({
        url: `/transactions`,
        method: 'GET',
        params: parameters,
      });

      dispatch({
        type: GET_TRANSACTIONS_SUCCESS,
        payload: { data: result, count: countR.length },
      });
    } catch (e) {
      dispatch({ type: GET_TRANSACTIONS_FAIL, payload: e });
    }
  };

export const getSingleTransaction = (id) => async (dispatch) => {
  dispatch({ type: GET_SINGLE_TRANSACTION_REQUEST });
  try {
    const result = await adapter({
      url: `/transactions/${id}`,
      method: 'GET',
    });

    dispatch({ type: GET_SINGLE_TRANSACTION_SUCCESS, payload: result });
  } catch (e) {
    dispatch({ type: GET_SINGLE_TRANSACTION_FAIL, payload: e });
  }
};
