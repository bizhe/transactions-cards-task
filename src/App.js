import React, { lazy, Suspense } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Loading from './components/Loading';
// prettier-ignore
const TransactionDetail = lazy(() => import("./pages/Transactions/TransactionDetail"));
const Transactions = lazy(() => import('./pages/Transactions/Transactions'));
const Cards = React.lazy(() => import('./pages/Cards/Cards'));
const CardDetail = React.lazy(() => import('./pages/Cards/CardDetail'));
const Layout = React.lazy(() => import('./components/Layout'));

function Home() {
  return (
    <div>
      <h1 className="mb-4">Home</h1>
    </div>
  );
}

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <Suspense fallback={<Loading />}>
              <Layout />
            </Suspense>
          }
        >
          <Route index element={<Home />} />
          <Route
            path="transactions"
            element={
              <Suspense fallback={<Loading />}>
                <Transactions />
              </Suspense>
            }
          />
          <Route
            path="transactions/:transactionId"
            element={
              <Suspense fallback={<Loading />}>
                <TransactionDetail />
              </Suspense>
            }
          />
          <Route
            path="transactions/:transactionId/:cardId"
            element={
              <Suspense fallback={<Loading />}>
                <CardDetail />
              </Suspense>
            }
          />
          <Route
            path="cards"
            element={
              <Suspense fallback={<Loading />}>
                <Cards />
              </Suspense>
            }
          />
          <Route
            path="cards/:cardId"
            element={
              <Suspense fallback={<Loading />}>
                <CardDetail />
              </Suspense>
            }
          />
          <Route
            path="cards/:cardId/transactions"
            element={
              <Suspense fallback={<Loading />}>
                <Transactions />
              </Suspense>
            }
          />
          <Route
            path="cards/:cardId/transactions/:transactionId"
            element={
              <Suspense fallback={<Loading />}>
                <TransactionDetail />
              </Suspense>
            }
          />
          <Route
            path="*"
            element={
              <main>
                <p className="display-1 mt-5">
                  The page you're looking for can't be found
                </p>
              </main>
            }
          />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
