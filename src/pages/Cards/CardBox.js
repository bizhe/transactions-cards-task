import React from 'react';
import PropTypes from 'prop-types';

import { formatDate } from '../../utils';

const CardBox = ({ data, onAction }) => {
  return (
    <div className="col-sm-4">
      <div className="card mb-4">
        <div className="card-body">
          <h5 className="card-title">Card</h5>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">
              <span className="fw-bold me-1">balance:</span> {data.balance}
            </li>
            <li className="list-group-item">
              <span className="fw-bold me-1">currency:</span>
              {data.currency}
            </li>
            <li className="list-group-item">
              <span className="fw-bold me-1">card number:</span>
              **** **** **** {data.maskedCardNumber}
            </li>
            <li className="list-group-item">
              <span className="fw-bold me-1">card account:</span>
              {data.cardAccount}
            </li>
            <li className="list-group-item">
              <span className="fw-bold me-1">card id:</span>
              {data.id}
            </li>
            <li className="list-group-item">
              <span className="fw-bold me-1">status:</span>
              {data.status}
            </li>
            <li className="list-group-item">
              <span className="fw-bold me-1">expire date:</span>
              {formatDate(data.expireDate)}
            </li>
          </ul>
          <button onClick={() => onAction(data.id)} className="btn btn-primary">
            Go detail
          </button>
        </div>
      </div>
    </div>
  );
};

CardBox.propTypes = {
  data: PropTypes.object,
  onAction: PropTypes.func,
};

export default CardBox;
